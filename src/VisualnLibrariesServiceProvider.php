<?php

namespace Drupal\visualn_libraries;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Adds custom namespaces.
 */
class VisualnLibrariesServiceProvider extends ServiceProviderBase {

  // @todo: add compiler pass instead
   public function register(ContainerBuilder $container) {
      //$container->addCompilerPass(new MyCustomExtensionPass());
   }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $namespaces = $container->getParameter('container.namespaces');
    // @todo: or better use addCompilerPass() instead
    $namespaces['visualn_libraries'] = 'components/visualn_libraries/src/';

    // @todo: check https://www.drupal.org/docs/8/api/services-and-dependency-injection/altering-existing-services-providing-dynamic-services
    //   for more info

    // @todo: add required namespaces
    $container->setParameter('container.namespaces', $namespaces);
  }
}

