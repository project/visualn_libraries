<?php

namespace Drupal\visualn_libraries\Core;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for VisualN Library plugins.
 */
abstract class LibraryBase extends PluginBase implements LibraryInterface {

  /**
   * Library files location path.
   *
   * The path should be used in libraries js and css files paths
   * in getLibrariesInfo() method to allow changing the location
   * where libraries assets are being stored.
   *
   * Defaults to '/visualn-libraries' directory.
   * @see visualn_libraries_library_info_build()
   */
  protected $base_path;

  /**
   * {@inheritdoc}
   */
  public function getBasePath() {
    return $this->base_path;
  }

  /**
   * {@inheritdoc}
   */
  public function setBasePath($base_path) {
    $this->base_path = $base_path;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getLibrariesInfo();

}
