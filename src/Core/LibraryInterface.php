<?php

namespace Drupal\visualn_libraries\Core;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for VisualN Library plugins.
 */
interface LibraryInterface extends PluginInspectionInterface {

  /**
   * Get base path for library files.
   */
  public function getBasePath();

  /**
   * Set base path for library files.
   */
  public function setBasePath($base_path);

  /**
   * Get libraries info array.
   *
   * The method generally returns libraries info to be registered by the
   * visualn_libraries module and to be made available to all other code as common
   * drupal visualn_libraries/[library_name] libraries.
   * Each library info set is keyed by the library machine name as it would be
   * in *.libraries.yml file.
   * Generally, each included file path (js or css) should have $this->base_path to be
   * appended to it if a common visualn_libraries assets directory is supposed to
   * be used for storing its files.
   * Also it allows to implement more complex logic, e.g. to switch between dev and prod
   * versions of assets (e.g. full and minimized js script versions) depending on some
   * setting value set in dev/prod environment.
   *
   * @todo: add code example here
   */
  public function getLibrariesInfo();

}
