<?php

namespace Drupal\visualn_libraries\Manager;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the VisualN Library plugin manager.
 */
class LibraryManager extends DefaultPluginManager {


  /**
   * Constructs a new VisualNLibraryManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/VisualN/Library', $namespaces, $module_handler, 'Drupal\visualn_libraries\Core\LibraryInterface', 'Drupal\visualn_libraries\Annotation\VisualNLibrary');

    $this->alterInfo('visualn_library_info');
    $this->setCacheBackend($cache_backend, 'visualn_library_plugins');
  }

}
