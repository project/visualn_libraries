<?php

namespace Drupal\visualn_libraries\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'visualn_libraries.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'visualn_libraries_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('visualn_libraries.settings');

    // @todo: validate the field value
    $form['base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library assets base path'),
      '#description' => $this->t('Libraries assets files location relative to the site installation root directory.'),
      '#default_value' => $config->get('base_path'),
      '#required' => TRUE,
    ];


    // @todo: add help text to description

    $form['libraries_info_server'] = [
      '#type' => 'url',
      '#title' => $this->t('Libraries info server'),
      '#default_value' => $config->get('libraries_info_server'),
      '#disabled' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $old_val = $this->config('visualn_libraries.settings')->get('base_path');
    // trim the value, remove spaces and slashes
    $new_val = trim($form_state->getValue('base_path'), ' /');

    $this->config('visualn_libraries.settings')
      ->set('base_path', $new_val)
      ->set('libraries_info_server', $form_state->getValue('libraries_info_server'))
      ->save();

    if ($new_val != $old_val) {
      // clear libraries cache on submit (only if config value changed)
      \Drupal::service('library.discovery.collector')->clear();
    }
  }

}
