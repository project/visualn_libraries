<?php

namespace Drupal\visualn_libraries\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a VisualN Library item annotation object.
 *
 * @see \Drupal\visualn_libraries\Plugin\VisualNLibraryManager
 * @see plugin_api
 *
 * @Annotation
 */
class VisualNLibrary extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
