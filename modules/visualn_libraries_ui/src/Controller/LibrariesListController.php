<?php

namespace Drupal\visualn_libraries_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Serialization\Yaml;
use Drupal\Component\Utility\UrlHelper;

/**
 * Class LibrariesListController.
 */
class LibrariesListController extends ControllerBase {

  /**
   * Libraries list content build array.
   *
   * @return array
   *   Return libraries list content.
   */
  public function build() {

    // @todo: add ajax controller with nojs behavior to download packages metadata

    // @todo: add visualn_library configuration entity type to store downloaded libraries metadata
    //   or just keep everthing in one config
    //   and reuse the data in hook_library_info_build()

    $libraryDiscovery = \Drupal::service('library.discovery');
    $list = $libraryDiscovery->getLibrariesByExtension('visualn_libraries');
    $registered_libraries = array_keys($list);

    $rows = [];
    ksort($list);
    foreach ($list as $name => $library_info) {
      $has_errors = FALSE;
      $js_files = '';
      $css_files = '';
      if (!empty($library_info['js'])) {
        $js_files = '<div><strong>js</strong>:</div>';
        $js_files .= '<ul>';
        foreach ($library_info['js'] as $js_item) {
          $file_path = $js_item['data'];
          if (file_exists($file_path)) {
          }
          elseif (UrlHelper::isExternal($file_path)) {
          }
          else {
            $file_path = '!' . $file_path;
            $has_errors = TRUE;
          }
          $js_files .= '<li>' . $file_path . '</li>';
        }
        $js_files .= '</ul>';
      }
      if (!empty($library_info['css'])) {
        $css_files = '<div><strong>css</strong>:</div>';
        $css_files .= '<ul>';
        foreach ($library_info['css'] as $css_item) {
          $file_path = $css_item['data'];
          if (file_exists($file_path)) {
          }
          elseif (UrlHelper::isExternal($file_path)) {
          }
          else {
            $file_path = '!' . $file_path;
            $has_errors = TRUE;
          }
          $css_files .= '<li>' . $file_path . '</li>';
        }
        $css_files .= '</ul>';
      }

      $files_markup = $js_files . $css_files;
      $files_column = ['data' => ['#markup' => $files_markup]];
      $version = !empty($library_info['version']) ? $library_info['version'] : '';
      $row = [
        'data' => [$name, $files_column, $version],
      ];
      if ($has_errors) {
        $row['class'] = ['color-error'];
      }
      $rows[] = $row;
    }

    $missing_libraries = [];

    // @todo: also register visualn_libraries with no data provided as generic libraries (if needed)
    $required_libraries_collections = $this->getRequriedVisualNLibraries();
    foreach ($required_libraries_collections as $extension_name => $required_libraries) {
      ksort($required_libraries);
      foreach ($required_libraries as $library_name => $library_info) {
        // replace visualn_libraries library prefix with an empty string
        // @todo: what about non-visualn_libraries libraries?
        $name = preg_replace('/^visualn_libraries\//', '', $library_name);
        if (in_array($name, $registered_libraries)) {
          continue;
        }
        // @todo: use extension name instead of machine name

/*
        $files_column = t('No data provided (required by: :extension_name)', [':extension_name' => $extension_name]);
        $version = !empty($library_info['version']) ? $library_info['version'] : '';
        $rows[] = [
          // @todo: check if info is provided
          'data' => [$name, $files_column, $version],
          'class' => ['color-error'],
        ];
*/
        $missing_libraries[$name][] =  [
          'extension_name' => $extension_name,
          'version' => !empty($library_info['version']) ? $library_info['version'] : '',
        ];
      }
    }

    // @todo: add *all* requiring extensions info (multiple extensions may require the same library)
    //   and requred versions, validate for potential conflicts,
    //   consider different versions of the same library required by different extensions,
    // @todo: and the same note for installed (present) libraries - resolve conflicts and show
    //   requiring extensions with corresponding versions
    foreach ($missing_libraries as $name => $extensions_info) {
      $required_by = '<div><strong>' . t('required by') . '</strong>:</div>';
      $required_by .= '<ul>';

      foreach ($extensions_info as $extension_info) {
        // @todo: sort extensions versions rows by name, e.g. in ::getRequriedVisualNLibraries()
        //    also consider modules and themes, if those should be grouped together
        $extension_version = !empty($extension_info['version'])
          ? "{$extension_info['extension_name']}: {$extension_info['version']}"
          : $extension_info['extension_name'];

        $required_by .= '<li>';
        $required_by .= $extension_version;
        $required_by .= '</li>';
      }

      $required_by .= '</ul>';


      $files_markup = '<div>' . t('No data provided') . '</div>';
      $files_markup .= '<br />';
      $files_markup .= $required_by;
      $files_column = ['data' => ['#markup' => $files_markup]];

      $rows[] = [
        'data' => [$name, $files_column, ''],
        'class' => ['color-error'],
      ];
    }


    // @todo: output libraries list table
    $libraries_table = [
      '#type' => 'table',
      // @todo: add Weight, Status and Operations columns
      //   add info on library info source: 'in-code/plugin/imported/no info'
      '#header' => array(t('Library name'), t('Info'), t('Version')),
      '#empty' => t('There are no VisualN Libraries yet.'),
      '#rows' => $rows,
    ];


    // @todo: add some instructions below the table, also provide info on different Type/Status/Source types
    return [
      'libraries_list' => $libraries_table,
      '#attached' => [
        'library' => [
          'visualn_libraries_ui/admin.styling',
        ],
      ],
    ];
  }

  /**
   * Get VisualN Libraries required by other modules.
   *
   * @todo: rename the method
   */
  protected function getRequriedVisualNLibraries() {
    $modules = \Drupal::service('module_handler')->getModuleList();
    $themes = \Drupal::service('theme_handler')->rebuildThemeData();
    //$libraryDiscovery = \Drupal::service('library.discovery');
    $extensions = array_merge($modules, $themes);
    $root = \Drupal::root();
    foreach ($extensions as $extension_name => $extension) {
      $library_file = $extension->getPath() . '/' . $extension_name . '.visualn_libraries.yml';
      if (is_file($root . '/' . $library_file)) {
        //$libraries[$extension_name] = $libraryDiscovery->getLibrariesByExtension($extension_name);

        // @todo: check if not empty, also implement as services
        //   see library.discovery, library.discovery.parser and library.discovery.collector services
        $libraries[$extension_name] = Yaml::decode(file_get_contents($root . '/' . $library_file)) ?: [];
      }
    }
    return $libraries;
  }

}
