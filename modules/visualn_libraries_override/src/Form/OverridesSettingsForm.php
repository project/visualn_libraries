<?php

namespace Drupal\visualn_libraries_override\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OverridesSettingsForm.
 */
class OverridesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'visualn_libraries_override.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'visualn_libraries_override_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('visualn_libraries_override.settings');

    // @todo: also  let user to see more info on overridden libraries (at least on dependencies)
    //   when "keep_dependencies" option may be needed (e.g. show and icon near "keep dependencies" checkbox)

    $overrides = $config->get('overrides');

    $num_overrides_value = $form_state->get('num_overrides');
    $form['#tree'] = TRUE;
    $form['container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="overrides-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    if (empty($num_overrides_value)) {
      $num_overrides_value = max(count($overrides), 1);
      $form_state->set('num_overrides', $num_overrides_value);
    }

    $form['container']['libraries'] = [
      '#type' => 'table',
      '#header' => [t('Base'), t('Override'), t('Keep dependencies'), t('Enabled')],
      //'#header' => array(t('Base'), t('Override'), t('Weight'), t('Enabled'), t('Keep dependencies')),
      '#empty' => t('There are no items yet.'),
    ];
    for ($i = 0; $i < $num_overrides_value; $i++) {
      // @todo: use autocomplete textfields and allow to switch to selects
      $form['container']['libraries'][$i]['base'] = [
        '#type' => 'textfield',
        '#autocomplete_route_name' => 'visualn_libraries_override.autocomplete',
        '#autocomplete_route_parameters' => [],
        '#default_value' => isset($overrides[$i]) ? $overrides[$i]['base'] : '',
      ];
      $form['container']['libraries'][$i]['override'] = [
        '#type' => 'textfield',
        '#autocomplete_route_name' => 'visualn_libraries_override.autocomplete',
        '#autocomplete_route_parameters' => [],
        '#default_value' => isset($overrides[$i]) ? $overrides[$i]['override'] : '',
      ];
      $form['container']['libraries'][$i]['keep_dependencies'] = [
        '#type' => 'checkbox',
        '#default_value' => isset($overrides[$i]) ? $overrides[$i]['keep_dependencies'] : FALSE,
      ];
      $form['container']['libraries'][$i]['enabled'] = [
        '#type' => 'checkbox',
        '#default_value' => isset($overrides[$i]) ? $overrides[$i]['enabled'] : FALSE,
      ];
    }

    $form['add_item'] = [
      '#type' => 'submit',
      '#submit' => ['::addOne'],
      '#value' => $this->t('Add item'),
      '#ajax'   => [
        'callback' => '::addmoreCallback',
        'wrapper'  => 'overrides-fieldset-wrapper',
      ],
    ];

    $form_state->setCached(FALSE);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit callback for adding new item to the list of overrides.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_overrides_value = $form_state->get('num_overrides');
    $add_button = $num_overrides_value + 1;
    $form_state->set('num_overrides', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback for adding new item to the list of overrides.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo: validate overrides configuration form
    parent::validateForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $overrides = [];
    $overrides_items = $form_state->getValue(['container', 'libraries']);
    if (!empty($overrides_items)) {
      foreach ($overrides_items as $item) {
        // @todo: in case if base or override is set, show an error
        //   also check values format (i.e. "module"/"library" regex)
        //   add to validateForm() callback
        if (empty($item['base']) || empty($item['override'])) {
          continue;
        }

        // @todo: trim values if needed
        $overrides[] = [
          'base' => $item['base'],
          'override' => $item['override'],
          'keep_dependencies' => $item['keep_dependencies'],
          'enabled' => $item['enabled'],
        ];
      }
    }

    // @todo: maybe use string numeric keys for config
    //   also set default value in install config to "{}" then
    $this->config('visualn_libraries_override.settings')
      ->set('overrides', $overrides)
      ->save();

    // clear libraries cache only
    \Drupal::service('library.discovery.collector')->clear();
  }

}
