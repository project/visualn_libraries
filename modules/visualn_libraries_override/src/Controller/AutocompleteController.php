<?php

namespace Drupal\visualn_libraries_override\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request, $count) {
    $results = [];
    $count = 8;

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = Unicode::strtolower(array_pop($typed_string));

      // @todo: validate typed_string

      list($typed_extension, $typed_library) = array_pad(explode('/', $typed_string), 2, null);

      // @todo: exclude extensions that have no registered libraries (or show a message instead)
      $module_list = array_keys(\Drupal::service('config.factory')->get('core.extension')->get('module'));
      $theme_list = array_keys(\Drupal::service('config.factory')->get('core.extension')->get('theme'));
      $extension_names = array_merge($module_list, $theme_list);
      sort($extension_names);

      if ($typed_extension) {
        // get results for typed extension or library name
        if (strpos($typed_string, '/') === FALSE) {
          $extension_names_matching = array_filter($extension_names, function($item) use ($typed_extension) {
            return strpos($item, $typed_extension) === 0;
          });

          // get list of matching extension names
          $extension_names_matching = array_slice($extension_names_matching, 0, $count);
          foreach ($extension_names_matching as $extension_name) {
            $results[] = [
              'value' => $extension_name . '/',
              'label' => $extension_name,
            ];
          }
        }
        else {
          if (in_array($typed_extension, $extension_names)) {
            $library_names = \Drupal::service('library.discovery')->getLibrariesByExtension($typed_extension);
            $library_names = array_keys($library_names);
            sort($library_names);

            if (!empty($typed_library)) {
              $library_names_matching = array_filter($library_names, function($item) use ($typed_library) {
                return strpos($item, $typed_library) === 0;
              });
            }
            else {
              $library_names_matching = $library_names;
            }

            // get list of matching library names
            $library_names_matching = array_slice($library_names_matching, 0, $count);
            foreach ($library_names_matching as $libraries_name) {
              $results[] = [
                'value' => $typed_extension . '/' . $libraries_name,
                'label' => $libraries_name,
              ];
            }
          }
        }
      }
    }

    return new JsonResponse($results);
  }

}
