<?php

namespace Drupal\visualn_libraries_example\Plugin\VisualN\Library;

use Drupal\visualn_libraries\Core\LibraryBase;

/**
 * Provides a 'Demo Library' VisualN library.
 *
 * @ingroup library_plugins
 *
 * @VisualNLibrary(
 *  id = "demo",
 *  label = @Translation("Demo"),
 * )
 */
class DemoLibrary extends LibraryBase {

  /**
   * {@inheritdoc}
   */
  public function getLibrariesInfo() {
    $info['visualn_demo_library'] = [
      'version' => '1.0.0',
      'dependencies' => [
        'core/backbone',
      ],
      'js' => [
        $this->base_path . '/demo_library/demo-library-script.js' => [],
      ],
    ];

    return $info;
  }

}
