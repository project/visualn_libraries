<?php

namespace Drupal\visualn_libraries_builder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining VisualN Library entities.
 */
interface VisualNLibraryInterface extends ConfigEntityInterface {

  /**
   * Get the list of library javascript files paths.
   *
   * @return array
   */
  public function getJsFiles();

  /**
   * Set the list of library javascript files paths.
   *
   * @param array $js_files
   *
   * @return $this
   */
  public function setJsFiles($js_files);

  /**
   * Get the list of library css files paths.
   *
   * @return array
   */
  public function getCssFiles();

  /**
   * Set the list of library css files paths.
   *
   * @param array $css_files
   *
   * @return $this
   */
  public function setCssFiles($css_files);

  /**
   * Get the list of library dependencies.
   *
   * @return array
   */
  public function getLibraryDependencies();

  /**
   * Set the list of library dependencies.
   *
   * @param array $library_dependencies
   *
   * @return $this
   */
  public function setLibraryDependencies($library_dependencies);

}
