<?php

namespace Drupal\visualn_libraries_builder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the VisualN Library entity.
 *
 * @ConfigEntityType(
 *   id = "visualn_library",
 *   label = @Translation("VisualN Library"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\visualn_libraries_builder\VisualNLibraryListBuilder",
 *     "form" = {
 *       "add" = "Drupal\visualn_libraries_builder\Form\VisualNLibraryForm",
 *       "edit" = "Drupal\visualn_libraries_builder\Form\VisualNLibraryForm",
 *       "delete" = "Drupal\visualn_libraries_builder\Form\VisualNLibraryDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\visualn_libraries_builder\VisualNLibraryHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "visualn_library",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/visualn/libraries/visualn_library/{visualn_library}",
 *     "add-form" = "/admin/visualn/libraries/visualn_library/add",
 *     "edit-form" = "/admin/visualn/libraries/visualn_library/{visualn_library}/edit",
 *     "delete-form" = "/admin/visualn/libraries/visualn_library/{visualn_library}/delete",
 *     "collection" = "/admin/visualn/libraries/visualn_library"
 *   }
 * )
 */
class VisualNLibrary extends ConfigEntityBase implements VisualNLibraryInterface {

  /**
   * The VisualN Library ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VisualN Library label.
   *
   * @var string
   */
  protected $label;

  /**
   * The list of library javascript files paths.
   *
   * @var array
   */
  protected $js_files;

  /**
   * The list of library css files paths.
   *
   * @var array
   */
  protected $css_files;

  /**
   * The list of library dependencies.
   *
   * @var array
   */
  protected $library_dependencies;

  /**
   * {@inheritdoc}
   */
  public function getJsFiles() {
    return $this->js_files;
  }

  /**
   * {@inheritdoc}
   */
  public function setJsFiles($js_files) {
    $this->js_files = $js_files;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles() {
    return $this->css_files;
  }

  /**
   * {@inheritdoc}
   */
  public function setCssFiles($css_files) {
    $this->css_files = $css_files;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryDependencies() {
    return $this->library_dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function setLibraryDependencies($library_dependencies) {
    $this->library_dependencies = $library_dependencies;
    return $this;
  }

}
