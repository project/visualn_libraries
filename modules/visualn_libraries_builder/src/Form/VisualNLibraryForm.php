<?php

namespace Drupal\visualn_libraries_builder\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VisualNLibraryForm.
 */
class VisualNLibraryForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $visualn_library = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $visualn_library->label(),
      '#description' => $this->t("Label for the VisualN Library."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $visualn_library->id(),
      '#machine_name' => [
        'exists' => '\Drupal\visualn_libraries_builder\Entity\VisualNLibrary::load',
      ],
      '#disabled' => !$visualn_library->isNew(),
    ];

    $form['js_files'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Javascript files paths'),
      '#default_value' => implode("\n", $visualn_library->getJsFiles()),
      // @todo: add some more explanations about relative and absolute paths
      '#description' => $this->t("List paths to js files, one per line"),
    ];
    $form['css_files'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CSS files paths'),
      '#default_value' => implode("\n", $visualn_library->getCssFiles()),
      // @todo: add some more explanations about relative and absolute paths
      '#description' => $this->t("List paths to css files, one per line"),
    ];
    $form['library_dependencies'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Library dependencies'),
      '#default_value' => implode("\n", $visualn_library->getLibraryDependencies()),
      '#description' => $this->t("List library dependencies, one per line"),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $visualn_library = $this->entity;
    $status = $visualn_library->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label VisualN Library.', [
          '%label' => $visualn_library->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label VisualN Library.', [
          '%label' => $visualn_library->label(),
        ]));
    }

    // clear libraries cache only
    \Drupal::service('library.discovery.collector')->clear();

    $form_state->setRedirectUrl($visualn_library->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo: perform the following checks:
    //  file names should be valid (do not contain backslash)
    //  no duplicates are present or maybe just remove them
    //  no files are used by both, js and css groups
    //  remove lines composed of whitespaces
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /*
      $js_files = $form_state->getValue('js_files');
      $js_files_list = array_filter(preg_split('/\r\n|\r|\n/', $js_files));

      $this->entity->set('js_files', $js_files_list);
    */

    $config_keys = ['js_files', 'css_files', 'library_dependencies'];
    foreach ($config_keys as $config_key) {
      $value = $form_state->getValue($config_key);
      // remove new lines
      // @todo: also remove empty lines
      $list = array_filter(preg_split('/\r\n|\r|\n/', $value));
      $this->entity->set($config_key, $list);
    }
  }

}
